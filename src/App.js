import React, { Component } from 'react';
import Routes from '../src/components/Routes';
import './index.css';
import Footer from './components/Footer';

class App extends Component {

  HandleFooter = () => {
    return (<Footer />)
  }
  componentWillMount() {
    this.HandleFooter()
  }
  render() {
    return (
      <div>
        <Routes />
        {this.HandleFooter()}
      </div>
    );
  }
}

export default App;
