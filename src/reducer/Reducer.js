export const SUBSCRIBE = 'SUBSCRIBE';
export const SUBSCRIBE_SUCCESS = 'SUBSCRIBE_SUCCESS';
export const SUBSCRIBE_FAIL = 'SUBSCRIBE_FAIL';

export default function reducer(state = {
  subscribeState: {},
}, action) {
  switch (action.type) {
    case SUBSCRIBE:
      return { ...state, loading: true };
    case SUBSCRIBE_SUCCESS:
      console.log(action.payload.data)
      return { ...state, loading: false, subscribeState: action.payload.data };
    case SUBSCRIBE_FAIL:
      return { ...state, loading: false, subscribeState: action.error.response.data };

    default:
      return state;
  }
}

export function subscribe(data) {
  return {
    type: SUBSCRIBE,
    payload: {
      request: {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `/subscribers`,
        credentials: 'include',
        data: data,
      }
    }
  };
}