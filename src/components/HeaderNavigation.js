import React from 'react';
import { MDBContainer, MDBCol, MDBRow } from 'mdbreact';

class HeaderNavigation extends React.Component {
  state = {
    user:{}
  }
  HandleGetUsername = () =>{
    console.log(this.props.user.user)
    this.setState({user:this.props.user.user})
  }

  componentDidMount(){
    this.HandleGetUsername()
  }

  HandleTriggerDashboard = () => {
    window.location.href = '/dashboard'
  }

  render(){
    return(
      <MDBContainer fluid className='remove-padding current-username'>
        <MDBCol>
          <span className='font-weight-bold user-click' onClick={()=>{this.HandleTriggerDashboard()}}>{this.state.user.username}</span>
        </MDBCol>
      </MDBContainer>
    );
  }
}

export default HeaderNavigation;
