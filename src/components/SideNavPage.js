import React from 'react';
import { MDBContainer, MDBCol, MDBView, MDBMask } from 'mdbreact';
import { connect } from 'react-redux';
import { currentUser, admin_getUsers } from '../reducer/Reducer'

class SideNavPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentUser: {},
      adminUser: {},
      userTools: '',
      user: {},
      person:{},
    }
  }

  HandleGetCurrentUser = async () => {
    await this.props.currentUser(localStorage.getItem('key'))
    this.setState({ currentUser: this.props.currentUserState.user, person: this.props.currentUserState.user.person })
    console.log(this.state.currentUser.person.firstname)
  }

  HandleGetUsername = async () => {
    await this.props.currentUser(localStorage.getItem('key'))
    this.setState({ user: this.props.currentUserState.user })
    console.log(this.props.currentUserState.user)
  }

  componentDidMount() {
    this.HandleGetCurrentUser()
    this.HandleGetUsername()
  }

  render() {
    return (
      <MDBContainer fluid id='main' className='dashboard-padding'>
        <div id='mySidenav' className='sidenav'>
          <MDBCol className='img-padding'>
            <MDBView>
              <div className='profile-pic'>
                <img
                  src='https://res.cloudinary.com/dl7mgal9c/image/upload/v1553586619/Card%20Exchange/FINAL_CE_LOGO_PNG_WHITE.png'
                  className='profile-pic1'
                  alt=''
                  height='10px'
                />
                 <img
                  src='https://res.cloudinary.com/dl7mgal9c/image/upload/v1553586619/Card%20Exchange/Image_122.png'
                  className='profile-pic2'
                  alt=''
                  height='10px'
                />
              </div>
              <MDBMask className='flex-center mt-5'>
                <p className='white-text mt-4 profile-name'>{this.state.person.firstname} {this.state.person.lastname}</p>
              </MDBMask>
              <MDBMask className='flex-center'>
                <p className='white-text email-user'>{this.state.user.email}</p>
              </MDBMask>
            </MDBView>
          </MDBCol>
          <UserTools path={this.props.location} />
        </div>
      </MDBContainer>
    );
  }
}

class UserTools extends React.Component {
  HandleLogoutUser = async () => {
    localStorage.clear('key')
    window.location.href = '/'
  }
  HandleActiveLink = (path) => {
    return (window.location.pathname == path) ? 'activeSidenav' : '';
  }
  HandleActiveLinkSellCard = (path, path1) => {
    return (window.location.pathname == path || window.location.pathname == path1) ? 'activeSidenav' : '';
  }

  render(){
    return(
      <div>
        <a className={this.HandleActiveLink('/dashboard')} href='/dashboard'>ACCOUNT</a>
        <a className={this.HandleActiveLink('/transaction_history')} href='/transaction_history'>TRANSCATION HISTORY</a>
        <a className={this.HandleActiveLinkSellCard('/sellitems','/singles')} href='/sellitems'>SELL ITEMS</a>
        <a href='#' onClick={() => { this.HandleLogoutUser() }}>LOGOUT</a>
      </div>
    )
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    admin_get_usersState: state.admin_get_usersState,
    currentUserState: state.currentUserState,
  };
};

const mapDispatchToProps = {
  admin_getUsers,
  currentUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideNavPage);
