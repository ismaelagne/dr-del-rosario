import React, { Component } from 'react';
import TopNavigation from './../TopNavigation';
import BannerSection from './sections/BannerSection';
import AboutSection from './sections/AboutSection';
import WhatIDoSection from './sections/WhatIDoSection';
import ServiceSection from './sections/ServiceSection';
import ContactSection from './sections/ContactSection';
import AssociationSection from './sections/AssociationSection';

class HomePage extends Component {
  render() {
    return (
      <React.Fragment>
        <TopNavigation />
        <BannerSection />
        <AboutSection />
        <WhatIDoSection />
        <ServiceSection />
        <AssociationSection />
        <ContactSection />
      </React.Fragment>
    )
  }
}

export default HomePage;