import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBMask } from 'mdbreact';

class ServiceSection extends React.Component {
  render() {
    return (
      <MDBContainer fluid className='service-section remove-padding' >
        <MDBRow center id='service'>
          <h1>Service Section</h1>
        </MDBRow>
      </MDBContainer>
    );
  }
}
export default ServiceSection;