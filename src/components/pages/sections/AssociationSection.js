import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBMask } from 'mdbreact';

class AssociationSection extends React.Component {
  render() {
    return (
      <MDBContainer fluid className='association-section remove-padding' >
        <MDBRow center>
          <h1>Association Section</h1>
        </MDBRow>
      </MDBContainer>
    );
  }
}
export default AssociationSection;