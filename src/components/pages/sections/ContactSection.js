import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBMask } from 'mdbreact';

class ContactSection extends React.Component {
  render() {
    return (
      <MDBContainer fluid className='contact-section remove-padding' >
        <MDBRow center id='contact'>
          <h1>Contact Section</h1>
        </MDBRow>
      </MDBContainer>
    );
  }
}
export default ContactSection;