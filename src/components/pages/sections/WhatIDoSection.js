import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBMask } from 'mdbreact';

class WhatIDoection extends React.Component {
  render() {
    return (
      <MDBContainer fluid className='do-section remove-padding' >
        <MDBRow center id='what'>
          <h1>What I do Section</h1>
        </MDBRow>
      </MDBContainer>
    );
  }
}
export default WhatIDoection;