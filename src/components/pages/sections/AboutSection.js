import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBMask } from 'mdbreact';

class AboutSection extends React.Component {
  render() {
    return (
      <MDBContainer fluid className='about-section remove-padding' >
        <MDBRow center id='about'>
          <h1>Who am I Section</h1>
        </MDBRow>
      </MDBContainer>
    );
  }
}
export default AboutSection;