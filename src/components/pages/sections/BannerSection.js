import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBView, MDBMask, MDBCarousel, MDBCarouselInner, MDBCarouselItem } from 'mdbreact';
// import ChooseGame from '../../ChooseGame';
import 'react-alice-carousel/lib/alice-carousel.css';
const bgMagic = 'https://res.cloudinary.com/dl7mgal9c/image/upload/v1552294891/Card%20Exchange/banner-bg.png';

class BannerSection extends React.Component {
  render() {
    return (
      <MDBContainer fluid className='remove-padding' >
        <MDBCarousel
          activeItem={1}
          length={3}
          showControls={true}
          showIndicators={true}
          className="z-depth-1"
        >
          <MDBCarouselInner>
            <MDBCarouselItem itemId="1">
              <MDBView>
                <img
                  className="d-block w-100"
                  src="https://res.cloudinary.com/dl7mgal9c/image/upload/v1556006536/Card%20Exchange/Carousel3.png"
                  alt="First slide"
                />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="2">
              <MDBView>
                <img
                  className="d-block w-100"
                  src="https://res.cloudinary.com/dl7mgal9c/image/upload/v1556006534/Card%20Exchange/Carousel2.png"
                  alt="Second slide"
                />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="3">
              <MDBView>
                <img
                  className="d-block w-100"
                  src="https://res.cloudinary.com/dl7mgal9c/image/upload/v1556006533/Card%20Exchange/Carousel1.png"
                  alt="Third slide"
                />
              </MDBView>
            </MDBCarouselItem>
          </MDBCarouselInner>
        </MDBCarousel>
      </MDBContainer>
    );
  }
}
export default BannerSection;