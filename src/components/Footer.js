import React from 'react';
import { MDBRow, MDBFooter, MDBCol } from 'mdbreact';

const Footer = () => {
  return (
    <MDBFooter className='footer-style fluid remove-margin'>
      <MDBRow className='footer-align'>
        <MDBCol md='9' className='text-right'>
          <MDBRow>
            <p className='copyright-text'>&copy; Copyright 2018 Card Exchange Co. All right reserved</p>
          </MDBRow>
        </MDBCol>
      </MDBRow>
    </MDBFooter>
  );
}

export default Footer;
