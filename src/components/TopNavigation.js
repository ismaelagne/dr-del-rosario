import React, { Component } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBIcon, MDBNavbarToggler, MDBNavItem, MDBContainer, MDBNavLink, toast } from 'mdbreact';
import { login, register, currentUser } from '../reducer/Reducer';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
class TopNavigation extends Component {
  render() {
    return (
      <MDBContainer fluid className='remove-padding'>
        <Router>
          <header>
            <MDBNavbar className='top-navigation' light expand='md'>
              <MDBNavbarBrand>
                <a href='/'>
                  <img alt='logo' className='' height='50px' src='' />
                </a>
              </MDBNavbarBrand>
              {/* <MDBNavbarToggler onClick={this.onClick} color='black' /> */}
              <MDBNavbarNav right className='pl-5'>
                <MDBNavItem className='pr-2 pl-2'>
                  <a href='#home' className='top-nav-link'>Home</a>
                </MDBNavItem>
                <MDBNavItem className='pr-2 pl-2'>
                  <a href='#about' className='top-nav-link'>About</a>
                </MDBNavItem>
                <MDBNavItem className='pr-2 pl-2'>
                  <a href='#what' className='top-nav-link'>What I do</a>
                </MDBNavItem>
                <MDBNavItem className='pr-2 pl-2'>
                  <a href='#service' className='top-nav-link'>Services</a>
                </MDBNavItem>
                <MDBNavItem className='pr-2 pl-2'>
                  <a href='#contact' className='top-nav-link'>Contact</a>
                </MDBNavItem>
              </MDBNavbarNav>
              <input
                type="text"
                className="top-navigation-input"
              />
              <MDBIcon icon="search" />
            </MDBNavbar>
          </header>
        </Router>
      </MDBContainer>
    );
  }
}

export default (TopNavigation);
