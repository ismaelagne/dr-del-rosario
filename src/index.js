import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import axios from "axios";
import multiClientMiddleware from "redux-axios-middleware";
import reducer from "./reducer/Reducer";
// import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router } from 'react-router-dom';

const client = axios.create({
  baseURL: 'https://card-exchange-api.herokuapp.com',
  // baseURL: 'https://card-exchange-api-dev.herokuapp.com',
  // baseURL: 'http://localhost:3000',
  responseType: "json",
});

const store = createStore(
  reducer,
  applyMiddleware(multiClientMiddleware(client))
);


ReactDOM.render(
  <Provider store={store}>
    <Router><App /></Router>
  </Provider>, document.getElementById('root')
);

registerServiceWorker();
