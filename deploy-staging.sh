#!/bin/bash


# apt-get update
# apt-get install python.pip -y
# pip install awsebcli

# Install AWS CLI
apt-get update
apt-get install -y -qq python python-dev python-pip ca-certificates
# pip install awsebcli --upgrade --ignore-installed
python --version
pip install awscli
aws --version

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID 
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY 
aws configure set region us-east-1

pip --version
pip install awsebcli
eb --version
eb init cardexchange --region us-east-1 -p node.js
eb use Cardexchange-staging-env
eb deploy